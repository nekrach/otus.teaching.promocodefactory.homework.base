﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> AddAsync(T entity)
        {
            if (entity == null)
                return Task.FromResult(false);
            Data.Add(entity);
            return Task.FromResult(true);            
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            var needDelete = Data.FirstOrDefault(x => x.Id == id);
            if(needDelete is { })
            {
                Data.Remove(needDelete);
                return Task.FromResult(true);
            }
            else
                return Task.FromResult(false);
        }

        public Task<bool> UpdateByIdAsync(Guid id, T entity)
        {
            var needUpdate = Data.FirstOrDefault(x => x.Id == id);

            if (needUpdate is { } && entity is { })
            {
                var index = Data.IndexOf(needUpdate);
                Data[index] = entity;
                return Task.FromResult(true);
            }
            else
                return Task.FromResult(false);
        }
    }
}