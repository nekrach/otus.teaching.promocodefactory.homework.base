﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequest : EmployeeResponse
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public new string FullName => $"{FirstName} {LastName}";
    }
}