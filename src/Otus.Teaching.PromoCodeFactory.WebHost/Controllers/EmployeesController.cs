﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Helpers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <remarks>
        /// Пример
        /// 
        ///     POST /Todo
        ///     {
        ///         "id": "0e7ad584-7788-4ab1-95a6-ca0a5b444cbb",
        ///         "email": "spam@gmail.com",
        ///         "roles": [
        ///         ],
        ///         "appliedPromocodesCount": 0,
        ///         "firstName": "Тёма",
        ///         "lastName": "Уничтожитель"
        ///     }
        /// 
        /// </remarks>
        /// <param name="employeeRequest">данные по сотруднику</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(EmployeeRequest employeeRequest)
        {
            var employee = employeeRequest.IntoEmployee();

            var result = await _employeeRepository.AddAsync(employee);

            return Ok(result);
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id">индификатор сотрудника</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _employeeRepository.DeleteByIdAsync(id);

            return Ok(result);
        }

        /// <summary>
        /// Обновить сотрудника
        /// </summary>
        /// /// <remarks>
        /// Пример
        /// 
        ///     POST /Todo
        ///     {
        ///         "id": "0e7ad584-7788-4ab1-95a6-ca0a5b444cbb",
        ///         "email": "NOTspam@gmail.com",
        ///         "roles": [
        ///         ],
        ///         "appliedPromocodesCount": 0,
        ///         "firstName": "Михаил",
        ///         "lastName": "Творитель"
        ///     }
        /// 
        /// </remarks>
        /// <param name="id">индификатор сотрудника</param>
        /// <param name="employeeRequest">данные по сотруднику</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Update(Guid id, EmployeeRequest employeeRequest)
        {
            var employee = employeeRequest.IntoEmployee();

            var result = await _employeeRepository.UpdateByIdAsync(id, employee);

            return Ok(result);
        }
    }
}