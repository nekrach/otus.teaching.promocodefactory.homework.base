﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers
{
    public static class EmloyeeExtention
    {
        /// <summary>
        /// Конфертация в модель сотрудника <see cref="Employee"/>
        /// </summary>
        /// <returns></returns>
        public static Employee IntoEmployee(this EmployeeRequest employeeResponse)
        {
            return new Employee()
            {
                Id = employeeResponse.Id,
                Email = employeeResponse.Email,
                Roles = employeeResponse.Roles.Select(x => new Role()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FirstName = employeeResponse.FirstName,
                LastName = employeeResponse.LastName, 
                AppliedPromocodesCount = employeeResponse.AppliedPromocodesCount
            };
        }
    }
}
